
//Get以外用
function apidev(){
  //URL
  var url='https://postman-echo.com/post';
  //送信するデータ
  var data = {"str":'This is data.'};
  //設定
  var options = {
    //method指定。ここでpost, putなどを指定
    'method' : 'post',
    //json指定
    'contentType': 'application/json',
    // Convert the JavaScript object to a JSON string.
    'payload' : JSON.stringify(data),
    //HTTPレスポンスエラー対策用。trueにしておくと、後でステータスコードなどを確認してエラー処理などを行える。
    //falseにすると、エラー発生時にそこでプログラムが停止してしまう
    'muteHttpExceptions': true
  };

  //WebAPIを叩く。レスポンスをresponceに代入
  var responce=UrlFetchApp.fetch(url, options);
  //ステータスコードが200なら
  if(responce.getResponseCode()==200){
    Logger.log(responce);

  }
  //200以外なら（エラーなら）
  else {
    Logger.log('Error: Status code is '+responce.getResponseCode());
    throw new Error('status code is not 200')
  }
}

//Get用
//シンプルでよければ、UrlFetchApp.fetch(url)だけで良い
function GetDev(){
  //URL
  var url='https://postman-echo.com/get';
  //パラメータ
  var param='?foo1=bar1';
  var url_and_param=url+param;
  //設定
  var options = {
    //HTTPレスポンスエラー対策用。trueにしておくと、後でステータスコードなどを確認してエラー処理などを行える。
    //falseにすると、エラー発生時にそこでプログラムが停止してしまう
    'muteHttpExceptions': true
  };

  //WebAPIを叩く。レスポンスをresponceに代入
  //muteHttpExceptionsがtrueなので404などのエラーでも実行継続される
  var responce=UrlFetchApp.fetch(url_and_param, options);
  //ステータスコードが200なら
  if(responce.getResponseCode()==200){
    Logger.log(responce);
    Logger.log("************************************************");
    var getData=JSON.parse(responce);
    Logger.log(getData.headers.host);
  }
  //200以外なら（エラーなら）
  else {
    Logger.log('Error: Status code is '+responce.getResponseCode());
    throw new Error('status code is not 200')
  }
}
